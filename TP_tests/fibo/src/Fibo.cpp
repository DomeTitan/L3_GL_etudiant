#include <assert.h>
#include <string>
#include "Fibo.hpp"

int fibo(int n, int f0, int f1) {
    if(f0 < 0){
        throw std::string("Erreur1");
    }

    if(f0 > f1){
        throw std::string("Erreur2");
    }

    if(n < 0){
        throw std::string("Erreur3");
    }

    return n <= 0 ? f0 : fibo(n - 1, f1, f1 + f0);
}

