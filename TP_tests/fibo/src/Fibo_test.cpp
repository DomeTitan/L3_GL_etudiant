//
// Created by quentin on 27/04/2020.
//

#include "Fibo.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLivre) { };

TEST(GroupLivre, Fibo_1)
{
    CHECK_EQUAL(fibo(1), 1);
    CHECK_EQUAL(fibo(2), 1);
    CHECK_EQUAL(fibo(3), 2);
    CHECK_EQUAL(fibo(4), 3);
    CHECK_EQUAL(fibo(5), 5);
}

TEST(GroupLivre, Fibo_2)
{
    CHECK_THROWS(std::string, fibo(-1));
    CHECK_THROWS(std::string, fibo(50));
}
