//
// Created by quentin on 08/04/2020.
//

#include <cmath>

double sinus(double x, double a, double b) {
    return sin(2 * M_PI * (a * x + b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
        emscripten::function("sinus", &sinus);
}
